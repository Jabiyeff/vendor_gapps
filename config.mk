#
# Copyright (C) 2018-2019 The Google Pixel3ROM Project
# Copyright (C) 2020 Raphielscape LLC. and Haruka LLC.
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#

ifeq ($(TARGET_BUILD_WITH_GAPPS), true)
# product/app
PRODUCT_PACKAGES += \
    GoogleContactsSyncAdapter

# product/priv-app
PRODUCT_PACKAGES += \
    Phonesky

# system_ext/priv-app
PRODUCT_PACKAGES += \
    GoogleServicesFramework

# PrebuiltGmsCore
PRODUCT_PACKAGES += \
    PrebuiltGmsCoreRvc \
    AndroidPlatformServices

PRODUCT_PACKAGES += \
    libprotobuf-cpp-full \
    librsjni

$(call inherit-product, vendor/google/gms/product/blobs/product_blobs.mk)
$(call inherit-product, vendor/google/gms/system/blobs/system_blobs.mk)
$(call inherit-product, vendor/google/gms/system_ext/blobs/system-ext_blobs.mk)
endif
